# Old School Snake

Simple game, born on JavaScrip just for fun and gift of nostalgia.

## Getting Started

To start use game on your own, clone repository and run "index.html" in your browser.
Yet, i don't create a site, for plaing it Simple, it coming soon.

### Command to clone via shell git.
```
git clone https://gitlab.com/Drake.aren/Old_School_Snake.git
```

### Prerequisites

It's simple JavaScrip project, base on HTML.
You can run it simle added code from 'index.html' on your own site as it will be need.
As soon as, i wrote it to run on Descktop from Node, but it's like you want.

## Authors

* **Drake.aren** - [Other Project](https://gitlab.com/Drake.aren)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
